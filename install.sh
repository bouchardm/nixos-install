echo "Sudoing..."

sudo -i

echo $(whoami)

echo "Checking boot type..."

ls /sys/frameware/efi


echo "Partitioning..."

parted --script /dev/sda -- mklabel msdos

parted  --script /dev/sda -- mkpart primary 1MiB -8GiB

parted  --script /dev/sda -- mkpart primary linux-swap -8GiB 100%

mkfs.ext4 -L nixos /dev/sda1

mkswap -L swap /dev/sda2

swapon /dev/sda2

mount /dev/disk/by-label/nixos /mnt


echo "Fetching config..."

nix-env -iA nixos.git nixos.git-secret nixos.gnupg

git clone https://gitlab.com/bouchardm/secrets.git

cd secrets

gpg --pinentry-mode=loopback --import private-key.gpg


cd ..

git clone https://gitlab.com/bouchardm/nixos.git

cd nixos

nixos-generate-config --root /mnt

cp /mnt/etc/nixos/hardware-configuration.nix .

git secret reveal

rm -fr /mnt/etc/nixos

cd ..

cp -r nixos /mnt/etc


echo "change boot vim /mnt/etc/nixos/boot/boot.nix"

# vim boot/boot.nix # choose boot type

echo "nixos-install"

# nixos-install
